# dnscrawler.parser

`dnscrawler.parser` - an R package for [dns-crawler](https://pypi.org/project/dns-crawler/) output processing.

## Requirements

Current version of this library uses [Apache Spark](https://spark.apache.org) to read and process a JSON file stored in [Apache Hadoop](https://hadoop.apache.org) HDFS.

Full lists of required R packages is specified specified in [DESCRIPTION](DESCRIPTION) file.

## Installation

```R
install.packages("devtools")
devtools::install_git("https://gitlab.labs.nic.cz/adam/dnscrawler.parser")
```

## Configuration

Before starting you need to configure Hadoop/Spark access. This package includes an example configuration, you can find it by running the following commands:

```R
system.file(file.path("conf", "spark.yml"),package = "dnscrawler.parser")
system.file(file.path("conf", "hadoop.yml"),package = "dnscrawler.parser")
```

## Usage

### Connecting to Spark

```R
# loading required libraries
require(dplyr)
require(sparklyr)
require(sparklyr.nested)
require(dnscrawler.parser)

# path to YML files with configuration
spark_config <- system.file(file.path("conf", "spark.yml"),package = "dnscrawler.parser")
hadoop_config <- system.file(file.path("conf", "hadoop.yml"),package = "dnscrawler.parser")

# reading configuration
config_spark <- spark_config(spark_config)
config_hadoop <- config::get(file=hadoop_config)

# setting system variables
Sys.setenv(HADOOP_CONF_DIR = config_hadoop$HADOOP_CONF_DIR)
Sys.setenv(SPARK_HOME = config_hadoop$SPARK_HOME)
Sys.setenv(HADOOP_USER_NAME = config_hadoop$HADOOP_USER_NAME)

# connecting to Spark
sc <- spark_connect(master="yarn-client", config=config_spark)
```
### Reading crawler data


```R
# HDFS path to a JSON file (dns-crawler output)
input <- "hdfs:///data/dns-crawler/year=2019/month=12/day=16/results.json"

# reading JSON file
tbl_crawler <- spark_read_json(sc,
                               "tbl_crawler",
                               input,
                               memory=F)
```


### Getting data

In order to extract data from a JSON file (dns-crawler output) there is a set of functions which match the pattern `get_*_data()` . The output `tibble` contains a domain name along with data.

#### Examples

```R
# DNS software for domains

get_dns_software_data(sc, tbl_crawler)
# Source: spark<tbl_5af669076ddns_software_ipv4> [?? x 6]
   domain                ns                   versionbind                         ip              vendor   version
   <chr>                 <chr>                <chr>                               <chr>           <chr>    <chr>  
 1 bm-czech.cz           ns1.thinline.cz.     PowerDNS Authoritative Server 4.1.6 91.239.200.243  PowerDNS 4.1.6  
 2 agroodbyt.cz          ns.skok.cz.          9.8.4-rpz2+rl005.12-P1              81.0.233.236    BIND     9.8.4  
 3 agroodbyt.cz          ns2.skok.cz.         9.8.4-rpz2+rl005.12-P1              89.185.253.3    BIND     9.8.4  
 4 agroodbyt.cz          ns3.skokcz.eu.       9.8.4-rpz2+rl005.12-P1              109.123.223.114 BIND     9.8.4  
 5 ccm-brno.cz           beta.ns.active24.cz. Knot DNS 2.8.5                      81.0.238.27     Knot DNS 2.8.5  
 6 ccm-brno.cz           gama.ns.active24.sk. Knot DNS 2.8.5                      93.188.1.228    Knot DNS 2.8.5  
 7 czech-translations.cz ns.wedos.cz.         off                                 46.28.104.67    unknown  unknown
 8 czech-translations.cz ns.wedos.eu.         off                                 164.138.27.146  unknown  unknown
 9 czech-translations.cz ns.wedos.com.        off                                 31.31.77.88     unknown  unknown
10 czech-translations.cz ns.wedos.net.        off                                 213.136.87.214  unknown  unknown
# ... with more rows

```

```R
# SOA RR for domains

get_soa_data(sc, tbl_crawler)
# Source: spark<tbl_5af2ce07610soa> [?? x 8]
   domain                  mname                rname                   serial     refresh retry expire  minimum
   <chr>                   <chr>                <chr>                   <chr>      <chr>   <chr> <chr>   <chr>  
 1 bm-czech.cz             ns1.thinline.cz.     hostmaster.bm-czech.cz. 1533053491 86400   300   604800  1800   
 2 agroodbyt.cz            ns.skok.cz.          hostmaster.skok.cz.     2016060988 21600   3600  2419200 3600   
 3 ccm-brno.cz             alfa.ns.active24.cz. hostmaster.active24.cz. 2017011804 10800   1800  1209600 3600   
 4 czech-translations.cz   ns.wedos.com.        wedos.wedos.com.        2018051101 3600    1800  2592000 3600   
 5 elseinternational.cz    ns1.ignum.com.       hostmaster.ignum.cz.    2019110200 10800   3600  604800  3600   
 6 findandwin.cz           ns.forpsi.net.       admin.forpsi.com.       2020041201 3600    1800  2592000 3600   
 7 hooriaandludek.cz       alfa.ns.active24.cz. hostmaster.active24.cz. 2020030714 10800   1800  1209600 3600   
 8 infocentrum-rakovnik.cz ns.forpsi.net.       admin.forpsi.com.       2020032601 3600    1800  2592000 3600   
 9 jakubtesarcik.cz        ns1.ignum.com.       hostmaster.ignum.cz.    2019110200 10800   3600  604800  3600   
10 lucette.cz              ns.wedos.net.        wedos.wedos.com.        2020040801 3600    1800  2592000 3600   
# ... with more rows
```

```R
# WEB content for domains

get_web_content_data(sc, tbl_crawler)
# Source: spark<tbl_5af53390347web_content> [?? x 3]
   domain               content                                                                                                  status
   <chr>                <chr>                                                                                                     <dbl>
 1 bm-czech.cz          "<!DOCTYPE html>\n<html lang=\"cs-CZ\">\n\n<head>\n\t<meta charset='UTF-8'>\n\t<meta name=\"viewport\" …    200
 2 agroodbyt.cz         "<!DOCTYPE html>\r\n<html lang=\"cs-CZ\">\r\n<head>\r\n<meta charset=\"UTF-8\" />\r\n<title>Agroodbyt B…    200
 3 ccm-brno.cz          "<html>\n\n<head>\n\t<base href=\"/\">\n\t<meta charset=\"utf-8\">\n\n\t<title>ACTIVE 24, s.r.o.</title…    200
 4 czech-translations.… "<!DOCTYPE html><html lang=\"en\"><head lang=\"en\"><meta charset=\"UTF-8\"><meta name=\"viewport\" con…    200
 5 elseinternational.cz "<!DOCTYPE html>\r\n<html xmlns:fb=\"http://ogp.me/ns/fb#\" lang=\"cs\">\r\n<head>\r\n    <meta http-eq…    200
 6 findandwin.cz        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n<html>\r\n<head>\r\n<title>The dom…    200
 7 hooriaandludek.cz    "<!DOCTYPE html>\n<html lang=\"en-US\" class=\"no-js\">\n<head>\n\t<meta charset=\"UTF-8\">\n\t<meta na…    200
 8 infocentrum-rakovni… "<!DOCTYPE html>\n<html lang=\"cs\">\n<head>\n\t<meta charset=\"UTF-8\">\n  <!--[if IE]><meta http-equi…    200
 9 jakubtesarcik.cz     "<!DOCTYPE html>\n<!--[if IE 8]>\r\n<html class=\"lt-ie10 lt-ie9 no-js\" prefix=\"og: https://ogp.me/ns…    200
10 lucette.cz           "<!DOCTYPE html>\n<html class=\"html\">\n <head>\n\n  <meta http-equiv=\"Content-type\" content=\"text/…    200
# ... with more rows
```


### Getting statistics

Data extracted using one of `get_*_data()` function can be passed to a proper `compute_*_stats()` function in order to compute desired statistics.


#### Examples

```R
# WEB software statistics - vednor along with software version

get_web_software_data(sc, tbl_crawler) %>% compute_web_software_stats(include_version=T)
# Source:     spark<?> [?? x 3]
# Ordered by: desc(domains)
   vendor        version domains
   <chr>         <chr>     <dbl>
 1 Apache        unknown   33007
 2 ngnix         unknown   21811
 3 unknown       unknown   14322
 4 Apache        2.4.25     5070
 5 ATS           unknown    3032
 6 OpenResty     unknown    2601
 7 Apache        2.4.10     2024
 8 ngnix         1.14.2     1569
 9 Microsoft-IIS 10.0       1290
10 Apache        2.2.22     1289
# ... with more rows
```

```R
# Name Server location statistics - by country for IPv6

get_ns_data(sc, tbl_crawler, 6) %>% compute_ns_location_stats(country)
# Source:     spark<?> [?? x 3]
# Ordered by: desc(domains)
   country domains servers
   <chr>     <dbl>   <dbl>
 1 CZ        72364     588
 2 DE        20288     294
 3 NL        18065     122
 4 IT        14395       6
 5 SE        14265      34
 6 US        11798    1598
 7 NO         3127      12
 8 FR         1274     179
 9 GB          379      32
10 SI          325       4
# ... with more rows
```

### Getting full statistics

There is also a set of functions which match the pattern: `get_*_full_stats()`. These functions can be used for data extraction and subsequent stats computations. The output will contain *full stats*, i.e. statistics for all possible subsets. For example *full stats* for DNS software will include both statistics for both **IPv4** and **IPv6**, for mail software it will include all possible ports: **25**, **465**, **587**.

#### Examples

```R
# DANE statistics for all defined mail server ports (25, 465, 587)

get_mail_dane_full_stats(sc, tbl_crawler)
# Source: spark<?> [?? x 2]
  domains  port
    <dbl> <dbl>
1    6961    25
2     256   465
3     288   587
```

```R
# DNS software statistics for IPv4 and IPv6

get_dns_software_full_stats(sc, tbl_crawler)
# Source: spark<?> [?? x 4]
   vendor   domains servers ipv  
   <chr>      <dbl>   <dbl> <chr>
 1 Knot DNS   30923     121 IPv4 
 2 unknown    30274    1930 IPv4 
 3 BIND        8839    1635 IPv4 
 4 PowerDNS    8561     591 IPv4 
 5 GLUX-DNS    3352      15 IPv4 
 6 NSD           90      22 IPv4 
 7 Knot DNS   29245      72 IPv6 
 8 unknown    27743    1075 IPv6 
 9 PowerDNS    6448     211 IPv6 
10 BIND        5538     334 IPv6 
# ... with more rows
```

## Included scripts

This packages includes some command line R scripts:

  * `get_stats.R` - compute stats and save the result to CSV files. These files can be subsequently processed by [scripts.hadoopexport](https://gitlab.labs.nic.cz/adam/scripts.hadoopexport/) and [scripts.hadoopimport](https://gitlab.labs.nic.cz/adam/scripts.hadoopimport/) scripts.
  
You can locate the scripts by running the following command:
```R
system.file(file.path("exec"),package = "dnscrawler.parser")
```

### Example usage
```bash
./get_stats.R \
  --input /user/hive/warehouse/dns-crawler/year=2020/month=4/day=23/20200423-100k.json \
  --output /home/data/output \
  --spark_config spark.yml \
  --hadoop_config hadoop.yml \
  -e /home/data/exports
  
Loading required package: stringr
Loading required package: sparklyr
Loading required package: sparklyr.nested
Loading required package: dnscrawler.parser
[2020-04-30 12:16:09] Reading Spark config
[2020-04-30 12:16:09] Reading Hadoop config
[2020-04-30 12:16:09] Reading export list
	ns_location_cc
	ns_location_as
	dns_software_vendor
	dns_software_vendor_version
	web_software_vendor
	web_software_vendor_version
	mail_software_vendor
	mail_dane
[2020-04-30 12:16:09] Connecting to Spark
[2020-04-30 12:16:21] Reading crawler data [input=/user/hive/warehouse/dns-crawler/year=2020/month=4/day=23/20200423-100k.json]
[2020-04-30 12:17:00] Timestamp=1587636000 [year=2020,month=4,day=23]
[2020-04-30 12:17:00] Processing: ns_location_cc
[2020-04-30 12:17:19] 	saving...
[2020-04-30 12:17:19] Processing: ns_location_as
[2020-04-30 12:17:29] 	saving...
[2020-04-30 12:17:29] Processing: dns_software_vendor
[2020-04-30 12:17:38] 	saving...
[2020-04-30 12:17:38] Processing: dns_software_vendor_version
[2020-04-30 12:17:47] 	saving...
[2020-04-30 12:17:47] Processing: web_software_vendor
[2020-04-30 12:17:49] 	saving...
[2020-04-30 12:17:49] Processing: web_software_vendor_version
[2020-04-30 12:17:52] 	saving...
[2020-04-30 12:17:52] Processing: mail_software_vendor
[2020-04-30 12:18:01] 	saving...
[2020-04-30 12:18:01] Processing: mail_dane
[2020-04-30 12:18:05] 	saving...
[2020-04-30 12:18:05] Disconnecting from Spark
NULL
```
