% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dns_software.R
\name{get_dns_software_full_stats}
\alias{get_dns_software_full_stats}
\alias{get_dns_software_full_stats.spark_connection}
\title{Get DNS software full statistics (IPv4+IPv6)}
\usage{
get_dns_software_full_stats(...)

\method{get_dns_software_full_stats}{spark_connection}(
  sc,
  tbl_in,
  include_version = FALSE,
  collect = FALSE,
  method = "rbind"
)
}
\arguments{
\item{sc}{A \code{spark_connection}.}

\item{tbl_in}{A \code{tibble} - input dataset (dns-crawler data).}

\item{include_version}{A \code{logical} indicating if software version should be included. Default: \code{FALSE}}

\item{collect}{A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}}

\item{method}{A \code{character} indicating output format, should be: \code{join} or \code{rbind}. Default: \code{rbind}}
}
\value{
A \code{tibble}.
}
\description{
Returns DNS software statistics.
}
\examples{
library(dnscrawler.parser)
sc <- sparklyr::spark_connect()
tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
get_dns_software_full_stats(sc, tbl_crawler)

}
