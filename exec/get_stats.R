#!/usr/bin/env Rscript

## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## Maciej Andziński <maciej.andzinski@nic.cz>
##
## 2019.12 - 2020.04
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---- arguments ----

option_list = list(
  optparse::make_option(c("-i", "--input"),   type="character",  default=NULL,  help="Input file (in HDFS)",   metavar="character"),
  optparse::make_option(c("--wc"),   type="character",  default=NULL,  help="Input file with web content prediction results (in HDFS)", metavar="character"),
  optparse::make_option(c("-o", "--output"), type="character",  default=NULL,  help="Output directory (local)", metavar="character"),
  optparse::make_option(c("-e", "--export_list"), type="character",  default=NULL,  help="Export list", metavar="character"),
  optparse::make_option(c("--spark_config"),  type="character",  default=NULL,  help="Spark config",  metavar="character"),
  optparse::make_option(c("--hadoop_config"),  type="character",  default=NULL,  help="Hadoop config",  metavar="character")
);

opt_parser = optparse::OptionParser(option_list=option_list);
opt = optparse::parse_args(opt_parser);

if(
  (!is.null(opt$input) & !is.null(opt$output) & !is.null(opt$export_list) & !is.null(opt$spark_config) & !is.null(opt$hadoop_config))
) {

  input <- opt$input
  wc <- opt$wc
  output <- opt$output
  export_list <- opt$export_list
  spark_config <- opt$spark_config
  hadoop_config <- opt$hadoop_config

} else {

  optparse::print_help(opt_parser)
  stop("Please supply: --input, --output, --export_list, --spark_config, --hadoop_config", call.=FALSE)

}
library(dplyr, warn.conflicts = FALSE)
library(stringr)
library(sparklyr)
library(sparklyr.nested)
library(dnscrawler.parser)
library(tidyr)

msg("Reading Spark config")
config_spark <- sparklyr::spark_config(spark_config)

msg("Reading Hadoop config")
config_hadoop <- config::get(file=hadoop_config)

msg("Reading export list")
exports <- readLines(export_list)
cat(paste0("\t",exports),sep="\n")

Sys.setenv(HADOOP_CONF_DIR = config_hadoop$HADOOP_CONF_DIR)
Sys.setenv(SPARK_HOME = config_hadoop$SPARK_HOME)
Sys.setenv(HADOOP_USER_NAME = config_hadoop$HADOOP_USER_NAME)

msg("Connecting to Spark")
sc <- sparklyr::spark_connect(master="yarn-client", config=config_spark)

msg("Reading crawler data [input=",input,"]")
tbl_crawler <- spark_read_json(sc,
                               "tbl_crawler",
                               input,
                               memory=T)


if(!is.null(wc)) {
  msg("Reading web content data [input=",wc,"]")
  tbl_wc <- spark_read_csv(sc,
                                "tbl_wc",
                                wc,
                                memory=T)
}


ymd <- str_match(input, "year=([0-9]{4})/month=([0-9]{1,2})/day=([0-9]{1,2})")

year <- ymd[1,2]
month <- ymd[1,3]
day <- ymd[1,4]

ts=as.numeric(as.POSIXct(sprintf("%s-%s-%s 12:00:00 UTC", year, month, day),tz="UTC"))

msg("Timestamp=",ts," [year=",year,",month=",month,",day=",day,"]")

for(export in exports) {
  msg("Processing: ", export)
  if(export=="crawler_ns_location_cc") tbl_export <- get_ns_location_full_stats(sc, tbl_crawler, country, collect=TRUE) %>% rename(cc="country") else
  if(export=="crawler_ns_location_as") tbl_export <- get_ns_location_full_stats(sc, tbl_crawler, as, collect=TRUE) else
  if(export=="crawler_ns_ipv") tbl_export <- get_ns_ipv_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_dns_software_vendor") tbl_export <- get_dns_software_full_stats(sc, tbl_crawler, include_version=FALSE, collect=TRUE) else
  if(export=="crawler_dns_software_vendor_version") tbl_export <- get_dns_software_full_stats(sc, tbl_crawler, include_version=TRUE, collect=TRUE) else
  if(export=="crawler_web_software_vendor") tbl_export <- get_web_software_full_stats(sc, tbl_crawler, include_version=FALSE, collect=TRUE) else
  if(export=="crawler_web_software_vendor_version") tbl_export <- get_web_software_full_stats(sc, tbl_crawler, include_version=TRUE, collect=TRUE) else
  if(export=="crawler_mail_software_vendor") tbl_export <- get_mail_software_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_mail_dane") tbl_export <- get_mail_dane_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_mail_dane_by_registrar") tbl_export <- get_mail_dane_by_registrar_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_web_cert_issuer_o") tbl_export <- get_web_cert_full_stats(sc, tbl_crawler, "issuer.O", collect=TRUE) else
  if(export=="crawler_web_cert_issuer_cn") tbl_export <- get_web_cert_full_stats(sc, tbl_crawler, "issuer.CN", collect=TRUE) else
  if(export=="crawler_web_cert_expiration") tbl_export <- get_web_cert_full_stats(sc, tbl_crawler, "expires_in", collect=TRUE) else
  if(export=="crawler_web_cert_validity_period") tbl_export <- get_web_cert_full_stats(sc, tbl_crawler, "validity_period", collect=TRUE) else
  if(export=="crawler_web_cert_hash_algorithm") tbl_export <- get_web_cert_full_stats(sc, tbl_crawler, "algorithm", collect=TRUE) else
  if(export=="crawler_web_tls_version") tbl_export <- get_web_tls_full_stats(sc, tbl_crawler, "version", collect=TRUE) else
  if(export=="crawler_web_tls_cipher_name") tbl_export <- get_web_tls_full_stats(sc, tbl_crawler, "cipher_name", collect=TRUE) else
  if(export=="crawler_web_tls_cipher_bits") tbl_export <- get_web_tls_full_stats(sc, tbl_crawler, "cipher_bits", collect=TRUE) else
  if(export=="crawler_dns_spf") tbl_export <- get_dns_spf_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_dns_dkim") tbl_export <- get_dns_dkim_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_web_asn") tbl_export <- get_web_asn_full_stats(sc, tbl_crawler, collect=TRUE) else
  if(export=="crawler_web_asn_wc") {
    if(exists("tbl_wc")) {
      tbl_export <- get_web_asn_wc_full_stats(sc, tbl_crawler, tbl_wc, collect=TRUE) 
    } else {
      warning(paste0("No web content file! Skipping ",export))
      next
    }
  } else
  if(export=="crawler_mail_asn") tbl_export <- get_mail_geoip_stats(sc, tbl_crawler, geoip="asn", collect=TRUE) else
  if(export=="crawler_mail_cc") tbl_export <- get_mail_geoip_stats(sc, tbl_crawler, geoip="country", collect=TRUE) %>% rename(cc="country") else
  stop("Unknown export: ", export)
  msg("\tsaving...")
  write_csv(tbl_export, output, export, ts)
}

msg("Disconnecting from Spark")
spark_disconnect(sc)
