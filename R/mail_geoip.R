## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## Maciej Andziński <maciej.andzinski@nic.cz>
##
## 2023.04 - 2023.05
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---- DATA ----

#' Get mail geoip data
#'
#' From dns-crawler output extracts geoip data for mail servers configured for a domain name
#'

#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' tbl_mail_geoip <- get_mail_geoip_data(sc, tbl_crawler)
#'
#' @rdname get_mail_geoip_data
#' @export
get_mail_geoip_data <- function(...) UseMethod("get_mail_geoip_data")


#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param memory A \code{logical} indicating if the output \code{tibble} shoult be cached in Spark. Default: \code{FALSE}
#' @param tbl_name A \code{character} - name for the table in Spark, if \code{NULL} a random name will be created. Default: \code{NULL}
#' @rdname get_mail_geoip_data
#' @method get_mail_geoip_data spark_connection
#' @export
get_mail_geoip_data.spark_connection <- function(sc, tbl_in, memory=FALSE, tbl_name=NULL) {

  if(is.null(tbl_name)) tbl_name <- paste0(random_string("tbl_"),"_mail_geoip")

  tbl_out <- tbl_in %>%
    sdf_select(domain,data=results.MAIL.banners) %>%
    filter(!is.null(data)) %>%
    sdf_unnest(data) %>%
    sdf_unnest(geoip) %>%
    sdf_register(tbl_name)

  if(memory) tbl_cache(sc,tbl_name)

  tbl(sc,tbl_name)
}



# ---- STATS ----


#' Compute mail geoip statistics
#'
#' Computes statistics for geoip. The output \code{tibble} contains the following columns: \code{country}/\code{asn} - geoip country or ASN, \code{domains} - number of domains with a mail server in country/ASN
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_mail_geoip_data(sc, tbl_crawler) %>% compute_mail_geoip_stats("asn")
#' 
#'
#' @rdname compute_mail_geoip_stats
#' @export
compute_mail_geoip_stats <- function(...) UseMethod("compute_mail_geoip_stats")


#' @param tbl_in A \code{tibble} - input dataset (mail geoip data).
#' @param geoip A \code{character} indicating geoip field for stats, can be: \code{country}, \code{asn}. Default: \code{country}
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_mail_geoip_stats
#' @method compute_mail_geoip_stats tbl_spark
#' @export
compute_mail_geoip_stats.tbl_spark <- function(tbl_in, geoip="country", collect=FALSE) {

  
  stopifnot(geoip %in% c("country","asn"))
  
  geoip <- as.name(geoip)
  
  tbl_out <- tbl_in %>%
    group_by(!! enexpr(geoip)) %>%
    summarise(domains=n_distinct(domain)) %>%
    arrange(desc(domains))

  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}


#' Get mail geoip statistics
#'
#' Returns mail geoip statistics for country or ASN
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_mail_geoip_stats(sc, tbl_crawler, "country")
#'
#' @rdname get_mail_geoip_stats
#' @export
get_mail_geoip_stats <- function(...) UseMethod("get_mail_geoip_stats")

#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param geoip A \code{character} indicating geoip field for stats, can be: \code{country}, \code{asn}. Default: \code{country}
#' @param collect A \code{logical} indicating if the output \code{tibble} should be collected from Spark. Default: \code{FALSE}
#' @rdname get_mail_geoip_stats
#' @method get_mail_geoip_stats spark_connection
#' @export
get_mail_geoip_stats.spark_connection <-  function(sc, tbl_in, geoip="country", collect=FALSE) {

  stopifnot(geoip %in% c("country","asn"))
  
  tbl_out <- get_mail_geoip_data(sc, tbl_in) %>%
    compute_mail_geoip_stats(geoip)
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
  
}

