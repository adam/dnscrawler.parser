## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## Maciej Andziński <maciej.andzinski@nic.cz>
##
## 2020.03 - 2020.04
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---- DATA ----

#' Get mail software data
#'
#' From dns-crawler output extracts mail server banner and uses it to detect software vendor
#'

#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' tbl_mail_software <- get_mail_software_data(sc, tbl_crawler)
#'
#' @rdname get_mail_software_data
#' @export
get_mail_software_data <- function(...) UseMethod("get_mail_software_data")

#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param port A \code{numeric} indicating mail server port, can be: \code{25}, \code{465} or \code{587} Default: \code{25}
#' @param memory A \code{logical} indicating if the output \code{tibble} shoult be cached in Spark. Default: \code{FALSE}
#' @param tbl_name A \code{character} - name for the table in Spark, if \code{NULL} a random name will be created. Default: \code{NULL}
#' @rdname get_mail_software_data
#' @method get_mail_software_data spark_connection
#' @export
get_mail_software_data.spark_connection <- function(sc, tbl_in, port=25, memory=FALSE, tbl_name=NULL) {

  stopifnot(port %in% c(25,465,587))

  if(is.null(tbl_name)) tbl_name <- paste0(random_string("tbl_"),"_mail_software")

  tbl_out <- tbl_in %>%
    sdf_select(domain,data=results.MAIL) %>%
    sdf_unnest(data) %>%
    sdf_unnest(banners) %>%
    sdf_unnest(banners) %>%
    rename(selected_port=as.character(port)) %>%
    sdf_unnest(selected_port) %>%
    sdf_select(domain,host,banner) %>%
    mutate(
      vendor = case_when(
        like(lower(banner), '% postfix%') ~ "Postfix",
        like(lower(banner), '% microsoft%') ~ "Microsoft",
        like(lower(banner), '% exim%') ~ "Exim",
        like(lower(banner), '% sendmail%') ~ "Sendmail",
        like(lower(banner), '% icewarp%') ~ "IceWarp",
        like(lower(banner), '% kerio%') ~ "Kerio",
        like(lower(banner), '% symantec%') ~ "Symantec",
        like(lower(banner), '% barracuda%') ~ "Barracuda",
        like(lower(banner), '% rblsmtpd%') ~ "rblsmtpd",
        like(lower(banner), '% qmail%') ~ "qmail",
        like(lower(banner), '% mailerq%') ~ "mailerq",
        like(lower(banner), '% haraka%') ~ "Haraka",
        TRUE ~ "unknown"
      )
    ) %>%
    sdf_register(tbl_name)

  if(memory) tbl_cache(sc,tbl_name)

  tbl(sc,tbl_name)
}



# ---- STATS ----


#' Compute mail software statistics
#'
#' Computes statistics for mail software vendors. The output \code{tibble} contains the following columns: \code{domains} - number of delegations, \code{hosts} - number of mail servers (unique hostnames).
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_mail_software_data(sc, tbl_crawler) %>% compute_mail_software_stats()
#'
#' @rdname compute_mail_software_stats
#' @export
compute_mail_software_stats <- function(...) UseMethod("compute_mail_software_stats")


#' @param tbl_in A \code{tibble} - input dataset (mail software data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_mail_software_stats
#' @method compute_mail_software_stats tbl_spark
#' @export
compute_mail_software_stats.tbl_spark <- function(tbl_in, collect=FALSE) {

  tbl_out <- tbl_in %>%
    group_by(vendor) %>%
    summarise(domains=n_distinct(domain),hosts=n_distinct(host)) %>%
    ungroup() %>%
    arrange(desc(domains))

  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}


#' Get mail software statistics
#'
#' Returns mail software full statistics (including ports: 25, 465, 587)
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_mail_software_full_stats(sc, tbl_crawler)
#'
#' @rdname get_mail_software_full_stats
#' @export
get_mail_software_full_stats <- function(...) UseMethod("get_mail_software_full_stats")

#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @param method A \code{character} indicating output format, currently only: \code{rbind} is supported. Default: \code{rbind}
#' @rdname get_mail_software_full_stats
#' @method get_mail_software_full_stats spark_connection
#' @export
get_mail_software_full_stats.spark_connection <-  function(sc, tbl_in, collect=FALSE, method="rbind") {

  stopifnot(method %in% c("rbind"))

  tbl_port25 <- get_mail_software_data(sc, tbl_in, port=25) %>%
    compute_mail_software_stats()

  tbl_port465 <- get_mail_software_data(sc, tbl_in, port=465) %>%
    compute_mail_software_stats()

  tbl_port587 <- get_mail_software_data(sc, tbl_in, port=587) %>%
    compute_mail_software_stats()

  if(method=="rbind") {

    tbl_out <- sdf_bind_rows(
      tbl_port25 %>% mutate(port=25),
      tbl_port465 %>% mutate(port=465),
      tbl_port587 %>% mutate(port=587)
    )
  }

  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}
