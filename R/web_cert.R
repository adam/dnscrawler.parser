## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## Maciej Andziński <maciej.andzinski@nic.cz>
##
## 2021.09 - 2021.10
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# ---- DATA ----

#' Get WEB certificate data
#'
#' Extracts web certificate data from dns-crawler output.
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' tbl_web_cert <- get_web_cert_data(sc, tbl_crawler)
#'
#' @rdname get_web_cert_data
#' @export
get_web_cert_data <- function(...) UseMethod("get_web_cert_data")

#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param memory A \code{logical} indicating if the output \code{tibble} shoult be cached in Spark. Default: \code{FALSE}
#' @param tbl_name A \code{character} - name for the table in Spark, if \code{NULL} a random name will be created. Default: \code{NULL}
#' @rdname get_web_cert_data
#' @method get_web_cert_data spark_connection
#' @export
get_web_cert_data.spark_connection <- function(sc, tbl_in, memory=FALSE, tbl_name=NULL) {

  if(is.null(tbl_name)) tbl_name <- paste0(random_string("tbl_"),"_web_cert")

  tbl_out <- tbl_in %>%
    sdf_select(domain,data=results.WEB.WEB4_443) %>%
    filter(!is.null(data)) %>%
    sdf_explode(data) %>%
    sdf_select(domain,cert=data.final_step.cert) %>%
    filter(!is.null(cert)) %>%
    sdf_explode(cert) %>%
    sdf_register(tbl_name)

  if(memory) tbl_cache(sc,tbl_name)

  tbl(sc,tbl_name)
}


# ---- STATS ----


#' Compute web certificate statistics
#'
#' For given dataset counts number of domains for given certificate attribute
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_data(sc, tbl_crawler) %>% compute_web_cert_stats(issuer.O)
#'
#' @rdname compute_web_cert_stats
#' @export
compute_web_cert_stats <- function(...) UseMethod("compute_web_cert_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param attribute A \code{character} with attribute name.
#' @param collect A \code{logical} indicating if the output \code{tibble} should be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_cert_stats
#' @method compute_web_cert_stats tbl_spark
#' @export
compute_web_cert_stats.tbl_spark <- function(tbl_in, attribute, collect=FALSE) {
  
  stopifnot(attribute %in% c("issuer.O","issuer.CN","algorithm","subject.O","subject.CN","expired","expires_in","validity_period"))
  
  field_name <- paste0("cert.",attribute)
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, selected_attribute=!! sym(field_name)) %>%
    group_by(selected_attribute) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    rename(!! sym(attribute):=selected_attribute) %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}


#' Compute web TLS statistics
#'
#' For given dataset counts number of domains for given TLS attribute
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_tls_data(sc, tbl_crawler) %>% compute_web_tls_stats(issuer.O)
#'
#' @rdname compute_web_tls_stats
#' @export
compute_web_tls_stats <- function(...) UseMethod("compute_web_tls_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param attribute A \code{character} with attribute name.
#' @param collect A \code{logical} indicating if the output \code{tibble} should be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_tls_stats
#' @method compute_web_tls_stats tbl_spark
#' @export
compute_web_tls_stats.tbl_spark <- function(tbl_in, attribute, collect=FALSE) {
  
  stopifnot(attribute %in% c("cipher_name","cipher_bits","version"))
  
  field_name <- paste0("tls.",attribute)
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, selected_attribute=!! sym(field_name)) %>%
    group_by(selected_attribute) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    rename(!! sym(attribute):=selected_attribute) %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}



#' Get WEB cert full statistics
#'
#' Returns WEB cert statistics
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_full_stats(sc, tbl_crawler)
#'
#' @rdname get_web_cert_full_stats
#' @export
get_web_cert_full_stats <- function(...) UseMethod("get_web_cert_full_stats")

#' @param sc A \code{spark_connection}.
#' @param tbl_in A \code{tibble} - input dataset (dns-crawler data).
#' @param attribute A \code{character} with attribute name.
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname get_web_cert_full_stats
#' @method get_web_cert_full_stats spark_connection
#' @export
get_web_cert_full_stats.spark_connection <-  function(sc, tbl_in, attribute, collect=FALSE) {
  
  tbl_out <- get_web_cert_data(sc, tbl_in) %>%
    compute_web_cert_stats(attribute)
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}





#' Compute web certificate expired statistics
#'
#' For given dataset counts number of domains with expired/not expired certificate.
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_data(sc, tbl_crawler) %>% compute_web_cert_expired_stats()
#'
#' @rdname compute_web_cert_expired_stats
#' @export
compute_web_cert_expired_stats <- function(...) UseMethod("compute_web_cert_expired_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_cert_expired_stats
#' @method compute_web_cert_expired_stats tbl_spark
#' @export
compute_web_cert_expired_stats.tbl_spark <- function(tbl_in, collect=FALSE) {
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, cert_expired=cert.expired) %>%
    group_by(cert_expired) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}



#' Compute web certificate algorithm statistics
#'
#' For given dataset counts number of domains for each certificate algorithm.
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_data(sc, tbl_crawler) %>% compute_web_cert_algorithm_stats()
#'
#' @rdname compute_web_cert_algorithm_stats
#' @export
compute_web_cert_algorithm_stats <- function(...) UseMethod("compute_web_cert_algorithm_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_cert_algorithm_stats
#' @method compute_web_cert_algorithm_stats tbl_spark
#' @export
compute_web_cert_algorithm_stats.tbl_spark <- function(tbl_in, collect=FALSE) {
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, cert_algorithm=cert.algorithm) %>%
    group_by(cert_algorithm) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}




#' Compute web certificate issuer statistics
#'
#' For given dataset counts number of domains for each certificate issuer (O attribute)
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_data(sc, tbl_crawler) %>% compute_web_cert_issuer_o_stats()
#'
#' @rdname compute_web_cert_issuer_o_stats
#' @export
compute_web_cert_issuer_o_stats <- function(...) UseMethod("compute_web_cert_issuer_o_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_cert_issuer_o_stats
#' @method compute_web_cert_issuer_o_stats tbl_spark
#' @export
compute_web_cert_issuer_o_stats.tbl_spark <- function(tbl_in, collect=FALSE) {
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, cert_issuer=cert.issuer.O) %>%
    group_by(cert_issuer) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}


#' Compute web certificate issuer statistics
#'
#' For given dataset counts number of domains for each certificate issuer (CN attribute)
#'
#' @return A \code{tibble}.
#' @examples
#' library(dnscrawler.parser)
#' sc <- sparklyr::spark_connect()
#' tbl_crawler <- spark_read_json(sc, "tbl_crawler", "/dns-crawler/result.json")
#' get_web_cert_data(sc, tbl_crawler) %>% compute_web_cert_issuer_cn_stats()
#'
#' @rdname compute_web_cert_issuer_cn_stats
#' @export
compute_web_cert_issuer_cn_stats <- function(...) UseMethod("compute_web_cert_issuer_cn_stats")

#' @param tbl_in A \code{tibble} - input dataset (web certificate data).
#' @param collect A \code{logical} indicating if the output \code{tibble} shoult be collected from Spark. Default: \code{FALSE}
#' @rdname compute_web_cert_issuer_cn_stats
#' @method compute_web_cert_issuer_cn_stats tbl_spark
#' @export
compute_web_cert_issuer_cn_stats.tbl_spark <- function(tbl_in, collect=FALSE) {
  
  tbl_out <- tbl_in %>%
    sdf_select(domain, cert_issuer=cert.issuer.CN) %>%
    group_by(cert_issuer) %>%
    summarise(domains=n_distinct(domain)) %>%
    ungroup() %>%
    arrange(desc(domains))
  
  if(collect) {
    tbl_out %>% collect()
  } else {
    tbl_out
  }
}

